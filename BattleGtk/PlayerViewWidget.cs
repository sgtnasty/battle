﻿using System;
using System.Text;
using Gtk;
using BattleEngine;

namespace BattleGtk
{
    [System.ComponentModel.ToolboxItem (true)]
    public class PlayerViewWidget : Gtk.Bin
    {
        private Player player;
        private string description;
        private bool expanded;

        public PlayerViewWidget (Player p, string description, bool expanded = false) : base()
        {
            this.player = p;
            this.description = description;
            this.expanded = expanded;
            build ();
        }

        private HBox buildAttrHBox (string attr)
        {
            HBox hb = new HBox (true, 1);
            int val = this.player.GetAttribute (attr).Current;
            Label attrLabel = new Label (string.Format ("<b>{0}:</b>", attr));
            attrLabel.Justify = Justification.Right;
            attrLabel.UseMarkup = true;
            hb.PackStart (attrLabel, true, false, 1);

            Label valLabel = new Label (val.ToString ());
            valLabel.Justify = Justification.Left;
            hb.PackStart (valLabel, true, false, 1);

            Adjustment adjustment = new Adjustment (val, 0.0, 20.0, 1.0, 2.0, 20.0);
            ProgressBar progressBar = new ProgressBar (adjustment);
            progressBar.TooltipText = val.ToString ();
            hb.PackEnd (progressBar, true, true, 1);

            return hb;
        }


        private void build ()
        {
            // Player Name expander
            Expander name_expander = new Expander ("");
            Label name_label = new Label (
                string.Format (this.description + ": <i>{0}</i>/{1} remaining HP {2}", 
                    this.player.Name,
                    this.player.Level,
                    this.player.HitPoints
                ));
            name_label.UseMarkup = true;
            name_label.TooltipText = this.player.Summarize ();
            name_label.UseMarkup = true;
            name_label.Yalign = 0.0f;
            name_expander.LabelWidget = name_label;

            VBox vbox = new VBox (true, 1);

            vbox.PackStart (this.buildAttrHBox("Attack"), true, false, 1);
            vbox.PackStart (this.buildAttrHBox("Defence"), true, false, 1);
            vbox.PackStart (this.buildAttrHBox("Armor"), true, false, 1);
            vbox.PackStart (this.buildAttrHBox("Power"), true, false, 1);
            vbox.PackStart (this.buildAttrHBox("Speed"), true, false, 1);
            vbox.PackStart (this.buildAttrHBox("Range"), true, false, 1);

            Label healthLabel = new Label ();
            string healthLabelString = "{0}";
            if (this.player.GetHealthStatus() == Player.PlayerHealth.Damaged)
                healthLabelString = "<span color=\"#cc6666\">{0}</span>";
            else if (this.player.GetHealthStatus() == Player.PlayerHealth.Dead)
                healthLabelString = "<span color=\"#ff0000\">{0}</span>";
            else if (this.player.GetHealthStatus() == Player.PlayerHealth.Healthy)
                healthLabelString = "<span color=\"#339900\">{0}</span>";
            healthLabel.Text = string.Format (healthLabelString, this.player.GetHealthStatus ().ToString ());
            healthLabel.UseMarkup = true;
            healthLabel.TooltipText = this.player.HitsTaken.ToString () + " hit(s) taken";
            vbox.PackEnd (healthLabel, true, false, 1);

            name_expander.Add (vbox);
            name_expander.Expanded = this.expanded;

            this.Add (name_expander);
        }

        protected override void OnSizeAllocated (Gdk.Rectangle allocation)
        {
            if (this.Child != null)
            {
                this.Child.Allocation = allocation;
            }
        }

        protected override void OnSizeRequested (ref Requisition requisition)
        {
            if (this.Child != null)
            {
                requisition = this.Child.SizeRequest ();
            }
        }
    }
}

