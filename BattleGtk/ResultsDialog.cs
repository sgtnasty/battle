﻿//
//  StatusDialog.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;
using Gtk;
using BattleEngine;

namespace BattleGtk
{
    public class ResultsDialog : Gtk.Dialog
    {
        public ResultsDialog (BattleStats stats) : base ()
        {
            //this.Modal = false;
            Button close = new Button ("_Close");
            this.AddActionWidget (close, ResponseType.Close);

            this.Title = "Battle Result";
            //this.SetDefaultSize (500, 400);

            // prep stats via Labels
            Label label1 = new Label("<b>Battle Stats</b>");
            label1.UseMarkup = true;
            this.VBox.PackStart (label1, false, false, 1);
            Label label2 = new Label (string.Format ("The winner is <i>{0}</i>",
                stats.Winner.Name));
            label2.UseMarkup = true;
            this.VBox.PackStart (label2, false, false, 1);
            Label label3 = new Label (string.Format (
                "Duration: {0} hrs, {1} min, {2} sec, {3} ms",
                stats.BattleDuration ().Hours, stats.BattleDuration ().Minutes, 
                stats.BattleDuration ().Seconds, 
                stats.BattleDuration ().Milliseconds)
            );
            label3.UseMarkup = true;
            this.VBox.PackStart (label3, false, false, 1);
            Label label4 = new Label (string.Format (
                "It took {0} players {1} turns to complete, which is {2} seconds in the simulation",
                stats.BattlePlayers, stats.BattleTurns, stats.SimulationSeconds)
            );
            label4.UseMarkup = true;
            this.VBox.PackStart (label4, false, false, 1);

            Label l5 = new Label (string.Format ("Hits: {0}  Misses {1}  Accuracy {2:F}%", 
                                                 stats.TotalHits, 
                                                 stats.TotalMisses, 
                                                 stats.HitAccuracy * 100.0f));
            this.VBox.PackStart (l5, false, false, 1);

            // this is a spacer label
            Label label97 = new Label ("");
            this.VBox.PackStart (label97, false, false, 1);

            PlayerViewWidget pvw = new PlayerViewWidget (stats.Winner, "Winner", true);
            this.VBox.PackStart (pvw, true, true, 1);

			// this is the bottom showing the log file path
            HBox hbox = new HBox (false, 1);
            Label label99 = new Label ("Log file for this battle can be found:");
            hbox.Add (label99);
            StringBuilder sb = new StringBuilder ();
            sb.Append ("file://");
            //sb.Append (Environment.CurrentDirectory);
            //sb.Append ("/");
            sb.Append (stats.LogFilename);
            LinkButton lb = new LinkButton(sb.ToString());
            hbox.Add (lb);
            this.VBox.PackEnd (hbox, false, false, 1);            
        }
    }
}
