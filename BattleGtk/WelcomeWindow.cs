﻿//
//  WelcomeWindow.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Gtk;

namespace BattleGtk
{
    public class WelcomeWindow
    {
        MessageDialog d;
        Window parentWindow;

        public WelcomeWindow (Window parent)
        {
            this.parentWindow = parent;
            this.build ();
        }

        private void build ()
        {
            string welcomeMessage = @"Welcome to Battle.

Battle is a turn based combat simulator. Plug in some variables and let the battle engine determine the outcome.

You will need to do a few things to get started.
1) Add at least 2 players to the game
2) Roll their attributes
3) Press the Battle button";
            this.d = new MessageDialog (this.parentWindow, DialogFlags.Modal, 
                MessageType.Other, ButtonsType.Close, welcomeMessage);
            
            this.d.Title = "Welcome";
        }

        public void Run ()
        {
            this.d.ShowAll ();
            this.d.Run ();
            this.d.Hide ();
            this.d.Dispose();
        }
    }
}

