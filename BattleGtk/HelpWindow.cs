﻿// 
//  HelpWindow.cs
//  
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//  
//  Copyright (c) 2016 Ronaldo Nascimento
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;
using System.IO;
using Gtk;

namespace BattleGtk
{
    public class HelpWindow : Gtk.Window
    {


        public HelpWindow () : base ("Help")
        {
            this.SetDefaultSize (400, 500);
            VBox vbox1 = new VBox (false, 1);
            vbox1.PackStart (this.createHeader ("Battle Help", "Documentation"), false, false, 1);
            vbox1.PackStart (this.createHelpContent (), true, true, 1);
            vbox1.PackEnd (this.createActionBar (), false, false, 1);
            this.Add (vbox1);
        }

        private Widget createHeader(string heading, string subheading)
        {
            VBox vb1 = new VBox (false, 1);

            Label headerLabel = new Label (heading);
            Pango.FontDescription headerFont = new Pango.FontDescription ();
            headerFont.Style = Pango.Style.Normal;
            headerFont.Weight = Pango.Weight.Ultrabold;
            headerFont.Size = (int)(18 * Pango.Scale.PangoScale);
            headerLabel.ModifyFont (headerFont);
            vb1.PackStart (headerLabel, false, false, 1);

            Label subheaderLabel = new Label (subheading);
            Pango.FontDescription subheaderFont = new Pango.FontDescription ();
            subheaderFont.Style = Pango.Style.Oblique;
            subheaderFont.Weight = Pango.Weight.Light;
            subheaderFont.Size = (int)(12 * Pango.Scale.PangoScale);
            subheaderLabel.ModifyFont (subheaderFont);
            vb1.PackEnd (subheaderLabel, false, false, 1);

            return vb1;
        }

        private Widget createHelpContent()
        {
            ScrolledWindow sw = new ScrolledWindow ();

            TextBuffer tb = new TextBuffer (null);

            tb.Text = @"
<h1>Battle</h1>
<p>A turn based battle simulation.</p>
";

            TextView tv = new TextView (tb);
            tv.Editable = false;
            sw.Add (tv);
            return sw;
        }

        private Widget createActionBar()
        {
            HBox hb = new HBox (false, 1);
            Button closeButton = new Button ("_Close");
            hb.PackStart (new VSeparator ());
            hb.PackEnd (closeButton);
            return hb;
        }

    }
}

