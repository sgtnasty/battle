﻿//
//  PrefsDialog.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;
using Gtk;
using BattleEngine;

namespace BattleGtk
{
    public class PrefsDialog : Gtk.Dialog
    {
        public PrefsDialog (BattleEngine.Game game) : base ()
        {
            this.gameEngine = game;
            this.build ();
        }

        private BattleEngine.Game gameEngine;

        private void build ()
        {
            this.Title = "Battle Preferences";

            Button save = new Button ("_Save");
            this.AddActionWidget (save, ResponseType.Ok);
            Button close = new Button ("_Close");
            this.AddActionWidget (close, ResponseType.Close);

            this.VBox.PackStart (this.build_property_editor ("MinStat", this.gameEngine.Prefs.MinStat, 0, 18));
            this.VBox.PackStart (this.build_property_editor ("MaxTurns", this.gameEngine.Prefs.MaxTurns, 10, 9999));
            this.VBox.PackStart (this.build_property_editor ("MapHeight", this.gameEngine.Prefs.MapHeight, 0, 9999));
            this.VBox.PackStart (this.build_property_editor ("MapLength", this.gameEngine.Prefs.MapLength, 0, 9999));
            this.VBox.PackStart (this.build_property_editor ("MapWidth", this.gameEngine.Prefs.MapWidth, 0, 9999 ));
        }

        private Widget build_property_editor (string labelName, int value, int min, int max)
        {
            HBox hbox = new HBox (false, 5);

            Label label = new Label (labelName);
            hbox.PackStart (label, false, false, 0);
            Entry entry = new Entry ();
            entry.Text = value.ToString ();
            entry.WidthChars = 5;
            hbox.PackStart (entry, true, false, 0);
            int step = (int)((double)max / 10.0);
            HScale scale = new HScale (min, max, step);
            scale.Value = (double) value;
            scale.WidthRequest = 100;
            scale.ChangeValue += (object o, ChangeValueArgs args) => {
                int ival = (int)scale.Value;
                entry.Text = ival.ToString ();
            };
            hbox.PackEnd (scale, true, true, 0);
            return hbox;
        }
    }
}

