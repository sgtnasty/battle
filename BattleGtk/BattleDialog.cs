﻿//
//  BattleDialog.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;
using System.Collections.Generic;
using Gtk;
using BattleEngine;
using BattleEngine.Actions;

namespace BattleGtk
{
    public class BattleDialog : Gtk.Dialog, IAttackReport
    {
        private List<Attack> attacks;
        private VBox scrolled_vbox;
        private ScrolledWindow scrolled_window;
        private Button closeButton;
        private Label playersLeftLabel;

        public BattleDialog () : base ()
        {
            this.attacks = new List<Attack> ();
            this.playersLeftLabel = new Label ("Players left:");
            this.AddActionWidget (this.playersLeftLabel, 9);
            this.closeButton = new Button ("_Close");
            this.AddActionWidget (this.closeButton, 0);
            closeButton.Clicked += delegate(object sender, EventArgs e) {
                this.Hide ();
            };

            this.Title = "Battle";
            this.SetDefaultSize (900, 300);

            this.scrolled_window = new ScrolledWindow ();
            this.scrolled_window.HscrollbarPolicy = PolicyType.Automatic;
            this.scrolled_window.VscrollbarPolicy = PolicyType.Always;
            this.scrolled_vbox = new VBox (false, 1);
            this.scrolled_window.AddWithViewport (this.scrolled_vbox);

            this.VBox.PackStart (scrolled_window);
        }

        public void ReportPlayersLeft (int playersLeftStanding)
        {
            this.playersLeftLabel.Text = string.Format ("Players Left: {0}", 
                                                        playersLeftStanding);
        }

        public void ReportAttack (Attack attack)
        {
            this.attacks.Add (attack);

            HBox hbox = new HBox (false, 5);
            PlayerViewWidget attacker_pvw = new PlayerViewWidget (attack.Attacker, "Attacker");
            PlayerViewWidget defender_pvw = new PlayerViewWidget (attack.Defender, "Defender");
            Label hits_label = new Label ();
            if (attack.DamageDone < 0) {
                hits_label.Text = string.Format ("<span color=\"#ff0000\"><b>hits</b></span> for <i>{0}</i> damage", attack.DamageDone);
            } else if (attack.DamageDone == 0) {
                hits_label.Text ="<span color=\"#ff9900\"><b>hits</b></span>, but can not penetrate armor";
            } else {
                hits_label.Text = "<span color=\"#0066ff\"><i>misses</i></span>";
            }
            hits_label.Yalign = 0.0f;
            hits_label.UseMarkup = true;
            Label turn_label = new Label ("Turn " + attack.TurnNumber);
            turn_label.Yalign = 0.0f;
            hbox.PackStart (turn_label, false, false, 1);
            hbox.PackStart (attacker_pvw, false, false, 1);
            hbox.PackStart (hits_label, false, false, 1);
            hbox.PackEnd (defender_pvw, false, false, 1);

            this.scrolled_vbox.PackStart (hbox);
            this.scrolled_vbox.ShowAll ();

            this.scrolled_window.CheckResize ();

            Adjustment a = this.scrolled_window.Vadjustment;
            a.Value = (a.Upper - a.PageSize);
            this.scrolled_window.Vadjustment = a;        
        }

        public void EnableCloseButton(bool enable)
        {
            this.closeButton.Sensitive = enable;
        }
    }
}

