// 
//  BattleGui.cs
//  
//  Author:
//       Ronaldo Nascimento <ronaldo1@users.sf.net>
//  
//  Copyright (c) 2011 Ronaldo Nascimento
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Reflection;
using Gtk;
using BattleEngine;

namespace BattleGtk
{
    public class BattleGui : IBattleGui, IConsole, IStatusUpdater
	{
		public BattleGui ()
		{
            this.players_store = new ListStore (typeof(string), typeof(string));
            this.weapons_store = new ListStore (typeof(string));
            this.armor_store  = new ListStore (typeof(string));
		}
		
		const string BuilderResource = "BattleGtk.Resources.BattleGtk.ui";
		const string BattleIconRes = "BattleGtk.Resources.battleicon00.jpg";
		
		private Builder builder;
		private Gtk.Window window1;
		private Gtk.ToolButton new_toolbutton;
		private Gtk.TextView console_textview;
		private Gtk.Statusbar statusbar1;
		private ToolButton addplayer_toolbutton;
		private ToolButton rollchar_toolbutton;
		private ToolButton battle_toolbutton;
        private ToolButton about_toolbutton;
        private ToolButton pref_toolbutton;
        private ToolButton help_toolbutton;
        private TreeView players_treeview;
        private TreeView weapons_treeview;
        private TreeView armor_treeview;

        private ListStore players_store;
        private ListStore weapons_store;
        private ListStore armor_store;
        private TreeIter players_iter;

        private BattleDialog battle_dialog;

		public BattleEngine.Game GameEngine
		{
			get;
			private set;
		}
		
		#region IConsole implementation
		public void ConsoleWrite (string message)
		{
			this.LogWrite (message);
			this.logfile.Flush ();

			TextIter end = this.console_textview.Buffer.EndIter;
			this.console_textview.Buffer.Insert 
				(ref end, message);
			TextMark m = this.console_textview.Buffer.CreateMark 
				(null, end, false);
			this.console_textview.ScrollMarkOnscreen (m);
		}

		public void ConsoleWriteLine (string message)
		{
			this.ConsoleWrite (message + System.Environment.NewLine);
		}

		public void Update ()
		{
			this.DoEvents ();
		}

		public void Clear ()
		{
			this.console_textview.Buffer.Clear ();
		}

		#endregion

		#region Logging to file
		private System.IO.StreamWriter logfile;
        private string logfilename;

        private string GetLocalApplicationDataFolder()
        {
            string localpath = string.Format ("{0}{1}", 
                                   Environment.GetFolderPath (Environment.SpecialFolder.LocalApplicationData),
                                   "/battle");
            if (!Directory.Exists (localpath)) {
                Directory.CreateDirectory (localpath);
            }
            return localpath;
        }

		private void LogOpen (string filename)
		{
			DateTime d = DateTime.Now;
            string datefilename = string.Format ("{0}-{1:yyyyMMdd}-{2:HHmmss}.log",
				                     filename, d, d);
            string localpath = GetLocalApplicationDataFolder ();
            this.logfilename = string.Format ("{0}{1}{2}", localpath, "/", datefilename);
            Console.WriteLine ("Log path = {0}", this.logfilename);
            logfile = new StreamWriter (this.logfilename);
		}

		private void LogWrite (string message)
		{
			logfile.Write (message);
		}

		private void LogClose ()
		{
			logfile.Close ();
		}
		#endregion

        #region IStatusUpdater implementation
        public uint Push (uint context_id, string text)
        {
            uint ct = this.statusbar1.Push (context_id, text);
            this.DoEvents ();
            return ct;
        }
        public void Pop (uint context_id)
        {
            this.statusbar1.Pop (context_id);
            this.DoEvents ();
        }
        #endregion

		#region IBattleGui implementation
		public void Init (Game game)
		{
			this.LogOpen ("battle");
			this.GameEngine = game;
			this.GameEngine.BattleGui = this;
			Gtk.Application.Init ();

            try {
                this.builder = new Builder ();
                Stream stream = this.GetType ().Assembly.GetManifestResourceStream (BuilderResource);
                StreamReader reader = new StreamReader(stream);
                string buffer = reader.ReadToEnd();
                reader.Close();
                this.builder.AddFromString (buffer);
                this.window1 = (Window) this.builder.GetObject ("window1");
                this.about_toolbutton = (ToolButton) this.builder.GetObject ("about_toolbutton");
                this.addplayer_toolbutton = (ToolButton) this.builder.GetObject ("addplayer_toolbutton");
                this.battle_toolbutton = (ToolButton) this.builder.GetObject ("battle_toolbutton");
                this.console_textview = (TextView) this.builder.GetObject ("console_textview");
                this.players_treeview = (TreeView) this.builder.GetObject ("game_treeview");
                this.weapons_treeview = (TreeView) this.builder.GetObject ("weapons_treeview");
                this.armor_treeview = (TreeView) this.builder.GetObject ("armor_treeview");
                this.new_toolbutton = (ToolButton) this.builder.GetObject ("new_toolbutton");
                this.pref_toolbutton = (ToolButton) this.builder.GetObject ("pref_toolbutton");
                this.rollchar_toolbutton = (ToolButton) this.builder.GetObject ("rollchar_toolbutton");
                this.help_toolbutton = (ToolButton) this.builder.GetObject ("help_toolbutton");
                this.statusbar1 = (Statusbar) this.builder.GetObject ("statusbar1");
            }
            catch (Exception e) {
                Console.WriteLine (e.ToString ());
                string.Format ("{0}: {1}\n{2}", e.GetType ().ToString (), e.Message, e.Source);
                MessageDialog d = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Close, e.Message);
                d.Title = e.GetType ().ToString ();
                d.Run ();
                Environment.Exit (1);
            }
			
			this.window1.SetDefaultSize (1000, 600);
			this.addplayer_toolbutton.TooltipText = "Add a player to the mix";
			this.rollchar_toolbutton.TooltipText = 
                "Roll attributes for all players";
			this.new_toolbutton.TooltipText = "Clear results to start a new battle";
			this.battle_toolbutton.TooltipText = "Start the battle!";
            this.about_toolbutton.TooltipText = "About Battle";
            this.pref_toolbutton.TooltipText = "Preferences";
            this.help_toolbutton.TooltipText = "Help";

			this.console_textview.ModifyBase (StateType.Normal, 
                new Gdk.Color (0, 0, 0));
			this.console_textview.ModifyText (StateType.Normal, 
                new Gdk.Color (196, 196, 196));

            // setup the game assets tree and model
            {
                this.players_treeview.Model = this.players_store;
                this.players_treeview.AppendColumn ("Players", new CellRendererText (), 
                    "text", 0);
                this.players_treeview.AppendColumn ("Status", new CellRendererText (), 
                    "text", 1);
                this.players_treeview.HeadersVisible = true;
                this.players_treeview.RowActivated += PlayerTreeview_RowActivated;

                this.weapons_treeview.Model = this.weapons_store;
                this.weapons_treeview.AppendColumn ("Weapons", new CellRendererText (), 
                    "text", 0);
                this.weapons_treeview.HeadersVisible = true;

                this.armor_treeview.Model = this.weapons_store;
                this.armor_treeview.AppendColumn ("Armor", new CellRendererText (), 
                    "text", 0);
                this.armor_treeview.HeadersVisible = true;
            }

			this.window1.Icon = Gdk.Pixbuf.LoadFromResource (BattleIconRes);
			
			// event handler config
			this.window1.DeleteEvent += HandleWindow1DeleteEvent;
			this.addplayer_toolbutton.Clicked += HandleAddplayer_toolbuttonClicked;
			this.rollchar_toolbutton.Clicked += HandleRollchar_toolbuttonClicked;
			this.battle_toolbutton.Clicked += HandleBattle_toolbuttonClicked;
			this.new_toolbutton.Clicked += HandleNew_toolbuttonClicked;
            this.about_toolbutton.Clicked += HandleAbout_toolbuttonClicked;
			this.new_toolbutton.Sensitive = false;
            this.pref_toolbutton.Clicked += HandlePrefs_toolbuttonClicked;
            this.help_toolbutton.Clicked += Handle_helpToolbuttonClicked;

            this.window1.Shown += Handle_Window1Shown;
			
			this.statusbar1.Push (0, "Ready");
        }

		public void Run ()
		{
			this.window1.ShowAll ();
			this.GameEngine.GuiLoaded ();

            try {
                this.GameEngine.Prefs.Load ();
            }
            catch (Exception exp) {
                this.ConsoleWriteLine ("ERROR: Unable to load preferences file");
                this.ConsoleWriteLine (string.Format ("{0}: {1} at {2}", exp.GetType ().ToString (),
                    exp.Message, exp.Source));
            }
            this.GameEngine.GameMap = new Map (this.GameEngine.Prefs.MapLength, 
                this.GameEngine.Prefs.MapWidth, 
                this.GameEngine.Prefs.MapHeight);
            

			Gtk.Application.Run ();
			this.LogClose ();
		}
		
		public void DoEvents ()
		{
            while (Gtk.Application.EventsPending ())
                Gtk.Application.RunIteration ();
		}

        public IAttackReport AttackReporter ()
        {
            return this.battle_dialog;
        }

        public void UpdatePlayerHealth (Player p)
        {
            this.players_store.Foreach (delegate (TreeModel model, TreePath path, TreeIter iter) {
                string iterName = (string) model.GetValue(iter, 0);
                if (p.Name.Equals(iterName)) {
                    model.SetValue(iter, 1, p.GetHealthStatus().ToString());
                    return true;
                }
                else
                    return false;
                }
            );
            this.DoEvents ();
        }

        public void ReportPlayersLeft (int playersLeftStanding)
        {
            if (this.battle_dialog != null) {
                this.battle_dialog.ReportPlayersLeft (playersLeftStanding);
            }
        }

		#endregion

        #region Event Handlers

        void Handle_Window1Shown (object sender, EventArgs e)
        {
            WelcomeWindow w = new WelcomeWindow (this.window1);
            w.Run ();
        }

        void HandleBattle_toolbuttonClicked (object sender, EventArgs e)
        {
            this.statusbar1.Push (1, "Initializing Battle Engine...");
            this.new_toolbutton.Sensitive = true;
            this.SetControlState (false);
            try
            {
                if (this.battle_dialog != null) {
                    this.battle_dialog.Dispose();
                    this.battle_dialog = null;
                }
                this.battle_dialog = new BattleDialog ();
                this.battle_dialog.Modal = false;
                this.battle_dialog.TransientFor = this.window1;
                this.battle_dialog.DestroyWithParent = true;
                this.battle_dialog.EnableCloseButton (false);
                this.battle_dialog.ShowAll ();
                this.DoEvents ();

                /////////////////////////////////////////////////////////
                BattleStats stats = this.GameEngine.RunBattle (this);
                /////////////////////////////////////////////////////////

                this.ReportPlayersLeft(this.GameEngine.PlayersLeftStanding());

                this.battle_dialog.EnableCloseButton (true);
                stats.LogFilename = this.logfilename;
                ResultsDialog status_dialog = new ResultsDialog (stats);
                status_dialog.ShowAll ();
                int i = status_dialog.Run ();
                if ((ResponseType)i == ResponseType.Close) {
                    status_dialog.Hide ();
                    status_dialog.Dispose ();
                }
            }
            catch (Exception exp)
            {
                MessageDialog d = new MessageDialog (
                    this.window1, DialogFlags.DestroyWithParent,
                    MessageType.Error,
                    ButtonsType.Close,
                    true,
                    "<b>{0}</b>: {1}\n<i>{2}</i>",
                    exp.GetType().ToString (),
                    exp.Message,
                    exp.Source);
                d.Title = "Error";
                d.Modal = true;
                this.ConsoleWriteLine (exp.StackTrace);
                d.Run ();
                d.Hide ();
                d.Dispose ();
            }
            this.SetControlState (true);
            this.statusbar1.Pop (1);
        }

        void HandleAbout_toolbuttonClicked (object sender, EventArgs e)
        {
            Gtk.AboutDialog d = new AboutDialog ();
            Assembly asm = Assembly.GetExecutingAssembly ();
            d.Title = asm.FullName;
            d.ProgramName = (asm.GetCustomAttributes (
                typeof (AssemblyTitleAttribute), false) [0]
                as AssemblyTitleAttribute).Title;
            //d.Title = "About";
            d.Version = asm.GetName ().Version.ToString ();
            d.Website = "https://gitlab.com/sgtnasty/battle";
            d.WebsiteLabel = "battle is hosted on gitlab.com";
            d.Artists = new string[] { "TPC"};
            d.Authors = new string[] { "Ronaldo Nascimento"};
            // TODO: this always comes back as an empty string
            d.Comments = (asm.GetCustomAttributes (
                typeof (AssemblyDescriptionAttribute), false) [0]
                as AssemblyDescriptionAttribute).Description;
            if (d.Comments == null || d.Comments.Length == 0)
                d.Comments = "A platform independent roleplaying game engine.";
            d.Copyright = (asm.GetCustomAttributes (
                typeof (AssemblyCopyrightAttribute), false) [0]
                as AssemblyCopyrightAttribute).Copyright;
            d.Documenters = new string[] { "Ronaldo Nascimento"};
            Gdk.Pixbuf icon = new Gdk.Pixbuf (null, 
                "BattleGtk.Resources.battleicon00.jpg");
            if (icon == null) {
                d.LogoIconName = "battle";
            } else {
                d.Logo = icon;
            }            
            d.License = "GPL v3\nhttps://www.gnu.org/licenses/gpl.html";
            d.Run ();
            d.Hide ();
            d.Dispose ();
        }


		void HandleRollchar_toolbuttonClicked (object sender, EventArgs e)
		{
			this.SetControlState (false);
			try 
			{
				this.GameEngine.RollAttributes ();
			}
			catch (Exception exp)
			{
				MessageDialog d = new MessageDialog (this.window1, 
                    DialogFlags.DestroyWithParent,
                    MessageType.Error,
                    ButtonsType.Close,
                    true,
                    "<b>{0}</b>: {1}\n<i>{2}</i>",
                    exp.GetType().ToString (),
                    exp.Message,
                    exp.Source);
				d.Title = "Error";
				d.Modal = true;
				this.ConsoleWriteLine (exp.StackTrace);
				d.Run ();
				d.Hide ();
				d.Dispose ();
			}
			this.SetControlState (true);
		}

        void HandleAddplayer_toolbuttonClicked (object sender, EventArgs e)
        {
            BattleEngine.RandomName rname = new BattleEngine.RandomName (
                this.GameEngine.RandomEngine);
            Player p = new Player (rname.GetRandomName ());
            this.GameEngine.AddPlayer (p);
            this.players_store.AppendValues(p.Name, p.GetHealthStatus().ToString());
        }

        void HandleNew_toolbuttonClicked (object sender, EventArgs e)
        {
            this.Clear ();
            this.GameEngine.Players.Clear ();
            this.new_toolbutton.Sensitive = false;
            this.ConsoleWriteLine ("");
            this.ConsoleWriteLine ("Battlefield cleared");
            this.players_store.Clear();
        }

        void HandlePrefs_toolbuttonClicked (object sender, EventArgs e)
        {
            PrefsDialog prefsDlg = new PrefsDialog (this.GameEngine);
            //prefsDlg.ParentWindow = this.window1.GdkWindow;
            //prefsDlg.SetPosition (WindowPosition.CenterOnParent);
            prefsDlg.Parent = this.window1;
            prefsDlg.WindowPosition = WindowPosition.CenterOnParent;
            prefsDlg.ShowAll ();
            int i = prefsDlg.Run ();
            if ((ResponseType)i == ResponseType.Ok) {
                //TODO: Save preferences here - need to set values of this.GameEngine.Prefs from prefsDlg
                this.ConsoleWriteLine ("Saving preferences...");
                try {
                    this.GameEngine.Prefs.Store ();
                }
                catch (Exception exp) {
                    this.ConsoleWriteLine ("ERROR: Unable to store preferences file");
                    this.ConsoleWriteLine (string.Format ("{0}: {1} at {2}", exp.GetType ().ToString (),
                        exp.Message, exp.Source));
                    MessageDialog d = new MessageDialog (null, DialogFlags.Modal, 
                        MessageType.Error, ButtonsType.Close, exp.Message);
                    d.Title = exp.GetType ().ToString ();
                    d.Run ();
                    d.Hide ();
                    d.Dispose ();
                }
            }
            prefsDlg.Hide ();
            prefsDlg.Dispose ();
        }

        void PlayerTreeview_RowActivated (object o, RowActivatedArgs args)
        {
            TreeIter ti = new TreeIter ();
            this.players_store.GetIter (out ti, args.Path);
            string name = (string) this.players_store.GetValue (ti, 0);

            Player p = new Player ("");
            this.GameEngine.Players.TryGetValue (name, out p);
            Dialog d = new Dialog ("Player Info", this.window1, DialogFlags.Modal);
            Label l1 = new Label (p.Name);
            l1.Xalign = 0.0f;
            Pango.FontDescription fd = new Pango.FontDescription ();
            fd.Size = (int)(18 * Pango.Scale.PangoScale);
            fd.Weight = Pango.Weight.Bold;
            l1.ModifyFont (fd);
            d.VBox.Add (l1);
            HSeparator hsep = new HSeparator ();
            d.VBox.Add (hsep);
            d.VBox.Add (new PlayerViewWidget (p, "Player", true));
            Button close = new Button ("_Close");
            d.AddActionWidget (close, ResponseType.Close);
            d.ShowAll ();
            d.Run ();
            d.Hide ();
            d.Dispose ();
        }

        void HandleWindow1DeleteEvent (object o, DeleteEventArgs args)
        {
            //TODO: dont quit if we are in the middle of a battle!?
            Gtk.Application.Quit ();
        }

        void Handle_helpToolbuttonClicked (object sender, EventArgs e)
        {
            //HelpWindow helpWindow = new HelpWindow ();
            //helpWindow.ShowAll ();
            WelcomeWindow w = new WelcomeWindow(this.window1);
            w.Run ();
        }
        #endregion

		void SetControlState (bool sensitive)
        {
            this.addplayer_toolbutton.Sensitive = sensitive;
            this.battle_toolbutton.Sensitive = sensitive;
            //this.new_toolbutton.Sensitive = sensitive;
            this.rollchar_toolbutton.Sensitive = sensitive;
        }
	}
}

