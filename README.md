# Battle

A turn based battle simulator, runs on Linux, macOS, and Windows.

## Introduction

Takes almost any number of combatants, randomizes thier stats, and sets them at
each other leaving one man standing as a winner.

Custom rules, based on 3D6 dice mechanic.

## Requirements

## Installation

I have created a launcher for Linux systems, including .desktop file and bash script. It currently installs into /usr/local 
Need some help testing the installer.

Download built binaries (https://sites.google.com/site/sgtnasty/battle)
Install mono libraries for target platform: (http://www.mono-project.com/download/)

Not ready for installation yet, fork project and compile with [MonoDevelop](http://www.monodevelop.com/)

### Linux

#### Build Steps

1. Clone the repo
2. Install mono
    `sudo apt install mono-runtime gtk-sharp2`
3. build the package `xbuild Battle.sln`
4. Run the installer
    `cd package`
    `sudo ./install.sh`
5. Look in the Games section of your desktop application menu
6. profit?

#### Install steps

Coming soon...


#### Screenshots

battle running on Fedora 24 in GNOME Shell 3.20

![Linux/GNOME Screenshot 1](https://gitlab.com/sgtnasty/battle/raw/master/media/Linux-screenshot-001.png)

Battle finished, showing the results dialog along with the turn dialog.

![Linux/GNOME Screenshot 1](https://gitlab.com/sgtnasty/battle/raw/master/media/Linux-screenshot-002.png)

Detail of the turn dialog.

![Linux/GNOME Screenshot 1](https://gitlab.com/sgtnasty/battle/raw/master/media/Linux-screenshot-003.png)

Detail of the player info dialog.

![Linux/GNOME Screenshot 1](https://gitlab.com/sgtnasty/battle/raw/master/media/Linux-screenshot-004.png)

### macOS

Download "Mono for Mac OS X is available as a Mac Package (.pkg)"

![Battle Screenshot on Mac OS X](https://gitlab.com/sgtnasty/battle/raw/master/media/MacOS-screenshot_005.png)

### Windows

Need Gtk# for .NET located http://www.mono-project.com/ download the "Mono for .NET" installer and the "GTK# for .NET" MSI package.

You must add Mono bin folder to your path:

`C:\Program Files (x86)\Mono\bin`

![Windows Screenshot 1](https://gitlab.com/sgtnasty/battle/raw/master/media/Windows-screenshot_006.png)

