﻿using System;

namespace BattleEngine
{
    public interface IStatusUpdater
    {
        uint Push (uint context_id, string text);
        void Pop (uint context_id);
    }
}

