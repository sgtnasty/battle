﻿//
//  BattleStats.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;

namespace BattleEngine
{
    public class BattleStats
    {
        private DateTime start;
        private DateTime finish;

        public BattleStats ()
        {
        }

        public void StartBattle()
        {
            this.start = DateTime.Now;
        }

        public void EndBattle()
        {
            this.finish = DateTime.Now;
        }

        public TimeSpan BattleDuration ()
        {
            long ticks = this.finish.Ticks - this.start.Ticks;
            return TimeSpan.FromTicks (ticks);
        }

        public int BattleTurns {
            get;
            set;
        }

        public int BattlePlayers {
            get;
            set;
        }

        public Player Winner {
            get;
            set;
        }

        public string LogFilename{
            get;
            set;
        }

		public int TotalHits {
			get;
			set;
		}

		public int TotalMisses {
			get;
			set;
		}

		public int TotalDamage {
			get;
			set;
		}

        public int SimulationSeconds {
            get {
                return this.BattleTurns * 6;
            }
        }

        public float HitAccuracy
        {
            get {
                float th = (float)this.TotalHits;
                float tm = (float)this.TotalMisses;
                float attempts = th + tm;
                return th / attempts;
            }
        }
    }
}

