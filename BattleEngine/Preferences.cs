﻿//
//  Preferences.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.IO;

namespace BattleEngine
{
    public class Preferences
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BattleEngine.Preferences"/> class.
        /// </summary>
        public Preferences ()
        {
            this.SetDefaults ();
        }

        private void SetDefaults ()
        {
            this.MinStat = 8;
            this.MapHeight = 100;
            this.MapLength = 100;
            this.MapWidth = 100;
            this.MaxTurns = 1000;
        }

        #region Persistence
        /// <summary>
        /// Store this instance.
        /// </summary>
        public void Store ()
        {
            throw new NotImplementedException ();
        }

        /// <summary>
        /// Load this instance.
        /// </summary>
        public void Load ()
        {
            // get default storage location
            var p = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            // try to load the serializes form of this class
            throw new NotImplementedException (p.ToString());
        }
        #endregion

        /// <summary>
        /// Gets or sets the max players.
        /// </summary>
        /// <value>The max players.</value>
        public int MinStat
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the length of the map.
        /// </summary>
        /// <value>The length of the map.</value>
        public int MapLength {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the map.
        /// </summary>
        /// <value>The width of the map.</value>
        public int MapWidth {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the map.
        /// </summary>
        /// <value>The height of the map.</value>
        public int MapHeight {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the max turns.
        /// </summary>
        /// <value>The max turns.</value>
        public int MaxTurns {
            get;
            set;
        }
    }
}

