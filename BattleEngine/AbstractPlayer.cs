// 
//  AbstractPlayer.cs
//  
//  Author:
//       Ronaldo Nascimento <ronaldo1@users.sf.net>
//  
//  Copyright (c) 2011 Ronaldo Nascimento
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace BattleEngine
{
	public abstract class AbstractPlayer
	{
		public AbstractPlayer (string name)
		{
			this.Name = name;
			this.Level = 1;
			this.ExperiencePoints = 0;
		}
		
		#region Core Attributes
		public string Name
		{
			get;
			protected set;
		}
		
		public int Age
		{
			get;
			set;
		}
		
		public enum GenderType
		{
			Male,
			Female
		};
		
		public GenderType Gender
		{
			get;
			set;
		}
		
		public double Height
		{
			get;
			set;
		}
		
		public double Weight
		{
			get;
			set;
		}
		
		public int ExperiencePoints
		{
			get;
			protected set;
		}
		public void AddXP (int xp)
		{
			this.ExperiencePoints += xp;
			this.LevelUp ();
		}
		public int Level
		{
			get;
			protected set;
		}
		public int HitPoints {
			get;
			set;
		}
		private void LevelUp ()
		{
			double v = this.ExperiencePoints/500.0;
			v += 1.0;
			double x = Math.Log (v);
			double l = Math.Floor(x);
			this.Level = (int)l;
		}
		#endregion
	}
}

