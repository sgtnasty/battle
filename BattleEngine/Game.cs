// 
//  Game.cs
//  
//  Author:
//       Ronaldo Nascimento <ronaldo1@users.sf.net>
//  
//  Copyright (c) 2011 Ronaldo Nascimento
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
using System;
using System.Collections.Generic;
using System.Text;
using BattleEngine.Actions;

namespace BattleEngine
{
	/// <summary>
	/// 
	/// </summary>
	public class Game
	{
		/// <summary>
		/// Constructs the Game engine
		/// </summary>
		public Game ()
		{
			this.RandomEngine = new Random (System.DateTime.Now.Millisecond);
			this.players = new Dictionary<string, Player>();
            this.Prefs = new Preferences ();
		}
		
		/// <summary>
		/// Gets the random number generator unique throuought the game
		/// </summary>
		public Random RandomEngine
		{
			get;
			private set;
		}
		
		/// <summary>
		/// The one and only console for logging to GUI client
		/// </summary>
		public IConsole Console
		{
			get;
			set;
		}
		
		/// <summary>
		/// The one and only GUI client implementation
		/// </summary>
		public IBattleGui BattleGui
		{
			get;
			set;
		}
		
        /// <summary>
        /// Gets the game map.
        /// </summary>
        /// <value>The game map.</value>
		public Map GameMap
		{
			get;
			set;
		}

        /// <summary>
        /// Gets the prefs.
        /// </summary>
        /// <value>The prefs.</value>
        public Preferences Prefs {
            get;
            private set;
        }
		
		private readonly Dictionary<string, Player> players;
		public Dictionary<string, Player> Players
		{
			get
			{
				return this.players;
			}
		}
		public void AddPlayer (Player player)
		{
			if (this.players.ContainsKey (player.Name)) 
			{
				this.Console.ConsoleWriteLine (string.Format (
					"Player {0} already exists and has been removed.", 
                    player.Name));
				return;
			}
			this.players.Add (player.Name, player);
			this.Console.ConsoleWriteLine (player.Summarize ());
		}
		public void RemovePlayer (Player player)
		{
			this.players.Remove (player.Name);
			this.Console.ConsoleWriteLine (player.Summarize () + " removed");
		}
		public int NumPlayers ()
		{
			return this.players.Count;
		}
		
		public void GuiLoaded ()
		{
			AssemblyAttributes gameaa = new AssemblyAttributes( this.GetType ());
			AssemblyAttributes guiaa = new AssemblyAttributes (
                this.BattleGui.GetType ());
			this.Console.ConsoleWriteLine (gameaa.TechDetails ());
			this.Console.ConsoleWriteLine (guiaa.TechDetails ());
		}
		
		private void roll_players_attributes (Player p)
		{
            double total = 0.0;
            int rolls = 0;
            double seeking = (6.0 * (double) this.Prefs.MinStat);
			while (total < seeking)
            {
                rolls++;
                total = (double) p.RollAttributes(this.RandomEngine);
            }
			this.Console.ConsoleWriteLine (p.GetRollDescription ());
		}
		
		private void place_players_on_map ()
		{
			this.GameMap.ClearPlayers ();
			this.Console.ConsoleWriteLine ("Placing players on map...");
			foreach (Player p in this.players.Values)
			{
				Location l = new Location (this.RandomEngine.Next (1, 
                    this.GameMap.Length+1),
				                           this.RandomEngine.Next (1, 
                        this.GameMap.Width+1),
				                           0);
				this.GameMap.SetPlayerOnMap (p, l);
				this.Console.ConsoleWriteLine (
                    string.Format("\"{0}\" is located at {1}", 
					p.Name, l.ToString ()));
			}
		}
		
		public void RollAttributes()
		{
			this.Console.ConsoleWriteLine ("Determining stats randomly...");
			if (this.players.Count < 1)
				throw new Exception (
                    "No players loaded into battle, add some players.");
			foreach (Player p in this.players.Values)
			{
				this.roll_players_attributes (p);
				//p.Level = 1;
				this.Console.ConsoleWriteLine (p.Summarize ());
			}
			
			this.place_players_on_map ();
		}

		public int PlayersLeftStanding ()
		{
			int left = 0;
			foreach (Player p in this.players.Values)
			{
				if (!p.IsDead())
					left ++;
			}
			return left;
		}

		private void print_final_stats()
		{
			this.Console.ConsoleWriteLine ("Final Stats for all players:");
			foreach (Player p in this.players.Values) {
				this.Console.ConsoleWriteLine (p.Summarize ());
			}
		}
        		
		public BattleStats RunBattle (IStatusUpdater status)
		{
			if (this.players.Count < 2)
				throw new Exception (
                    "Need at least 2 players to battle, add more players.");
            BattleStats stats = new BattleStats ();
            stats.StartBattle ();
            stats.BattlePlayers = this.players.Count;
            stats.BattleTurns = 1;
			this.Console.ConsoleWriteLine ("==========================");
			this.Console.ConsoleWriteLine ("=   THE BATTLE BEGINS!   =");
			this.Console.ConsoleWriteLine ("==========================");
            status.Push (1, "Battle begins");

			// initial targetting
			this.Console.ConsoleWriteLine ("");
			this.Console.ConsoleWriteLine (string.Format (
				"Initializing targeting for {0} players", this.players.Count));
            status.Push (2, "Selecting targets for all combatants, this may take a while...");
			foreach (Player p in this.players.Values)
			{
				p.Target = null;
				SelectTarget st = new SelectTarget (
                    p, this.players, this.GameMap, this.Console);
				p.Target = this.players[(string) st.Perform (stats)];
				this.Console.ConsoleWriteLine (
                    string.Format ("\"{0}\" targets \"{1}\"", 
					p.Name, p.Target.Name));
			}
            status.Pop (2);
			this.Console.ConsoleWriteLine ("");
			
			Player winner = null;
			int rounds = 1;

            status.Push (1, "Starting battle...");
			while (this.PlayersLeftStanding() > 1)
			{
				this.Console.ConsoleWriteLine ("");
				this.Console.ConsoleWriteLine ("ROUND " + rounds.ToString());
				this.BattleGui.DoEvents ();
				foreach (Player p in this.players.Values)
				{
                    // determine players left standing
                    this.BattleGui.ReportPlayersLeft (this.PlayersLeftStanding ());

                    status.Push (2, string.Format("{0}'s turn...", p.Name));
					if (!p.IsDead())
					{
						if (!p.Target.IsDead())
						{
                            status.Push (3, string.Format("{0} is attacking {1}", p.Name, p.Target.Name));
							Attack attack = new Attack (p, p.Target, 
                                this.GameMap, this.Console, rounds);
                            if ((int) attack.Perform (stats) < 0)
                                this.BattleGui.UpdatePlayerHealth (p.Target);
                            this.BattleGui.AttackReporter ().ReportAttack (attack);
                            this.BattleGui.DoEvents ();
                            status.Pop (3);
						}
						else if (this.PlayersLeftStanding () > 1)
						{
							SelectTarget st = new SelectTarget (p, 
                                this.players, this.GameMap, this.Console);
							p.Target = this.players[(string) st.Perform (stats)];
						}
					}
                    status.Pop (2);
				}
				// show current stats
				foreach (Player p in this.players.Values)
				{
					if (!p.IsDead())
						this.Console.ConsoleWriteLine (p.SummarizeHealth ());
				}
				rounds ++;
                stats.BattleTurns = rounds;
                if (rounds > this.Prefs.MaxTurns)
				{
					this.Console.ConsoleWriteLine ("Battle has lasted too long");
					winner = null;
					break;
				}
			}
            status.Pop (1);
            status.Push (1, "Determining winner...");
			foreach (Player p in this.players.Values)
			{
				if (!p.IsDead()) winner = p;
			}
            status.Pop (1);
            stats.EndBattle ();
			this.Console.ConsoleWriteLine ("==========================");
			this.Console.ConsoleWriteLine ("=    THE BATTLE ENDS!    =");
			this.Console.ConsoleWriteLine ("==========================");
            this.Console.ConsoleWriteLine ("");
			if (winner != null)
			{
                stats.Winner = winner;
				this.Console.ConsoleWriteLine (
                    string.Format("\"{0}\" wins the battle in {1} rounds!", 
					winner.Name, rounds));
				winner.AddXP (1000 * (this.players.Count - 1));
				//this.Console.ConsoleWriteLine (winner.Summarize ());
				this.Console.ConsoleWriteLine (
					string.Format("{0} has gained {1} XP and is level {2}", 
						winner.Name, winner.ExperiencePoints, winner.Level));
				this.print_final_stats();
			}
			else
				this.Console.ConsoleWriteLine ("Its a draw");
			//this.players.Clear ();			
            return stats;
		}
	}
}

