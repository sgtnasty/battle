﻿//
//  SelectTarget.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;

namespace BattleEngine.Actions
{
	public class SelectTarget : IAction
	{
		private Player source;
		private Dictionary<string, Player> ptargets;
		private Map gamemap;
		private IConsole console;

		public SelectTarget (Player source, Dictionary<string, Player> ptargets, 
            Map gamemap, IConsole console)
		{
			this.source = source;
			this.ptargets = ptargets;
			this.gamemap = gamemap;
			this.console = console;
		}

		public object Perform (BattleStats stats)
		{
			// TODO: put this is a thread or optimize, 
			//       large pool of players slows down immensely
			//       also how do we use the stats here?
			List<Player> potential_targets = new List<Player>();
			foreach (Player p in this.ptargets.Values)
			{
				if ((source != p) && (!p.IsDead()))
					potential_targets.Add (p);
			}
			Location myloc = this.gamemap.GetPlayerOnMap (source);
			Dictionary<Player, double> distances = new Dictionary<Player, double>();
			foreach (Player target in potential_targets)
			{
				Location tgtloc = this.gamemap.GetPlayerOnMap(target);
				distances.Add (target, myloc.DistanceTo (tgtloc));
			}
			double lowest_dist = (double)(this.gamemap.Length + 
				this.gamemap.Width + this.gamemap.Height) * 2;
			Player target_player = null;
			foreach (KeyValuePair<Player, double> kvp in distances)
			{
				this.console.Update ();
				this.console.ConsoleWriteLine (
					string.Format("\"{0}\" distance to \"{1}\" is {2:F1}", 
						source.Name, kvp.Key.Name, kvp.Value));
				if (kvp.Value < lowest_dist)
				{
					lowest_dist = kvp.Value;
					target_player = kvp.Key;
				}
			}
			return target_player.Name;
		}
	}
}

