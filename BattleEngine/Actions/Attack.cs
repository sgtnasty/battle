﻿//
//  Attack.cs
//
//  Author:
//       Ronaldo Nascimento <sgtnasty@gmail.com>
//
//  Copyright (c) 2016 Ronaldo Nascimento
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using BattleEngine;

namespace BattleEngine.Actions
{
	public class Attack : IAction
	{
		private Player attacker;
		private Player defender;
		private IConsole console;
		private Map gamemap;
        private int damage_done;
        private int turn;

        public Attack (Player attacker, Player defender, Map map, IConsole console, int turnno)
		{
			this.attacker = attacker;
			this.defender = defender;
			this.console = console;
			this.gamemap = map;
            this.turn = turnno;
		}

		public object Perform (BattleStats stats)
		{
            //TODO: this needs to be redone in 1 simple method to determine range
			Location sloc = this.gamemap.GetPlayerOnMap (attacker);
			Location tloc = this.gamemap.GetPlayerOnMap (defender);
			double dist = sloc.DistanceTo (tloc);
			int range = attacker.GetAttribute ("Range").Current;

            this.damage_done = 0;

			if ((double)dist <= range)
			{
				int attackroll = attacker.ThrowForAttack ();
				int defenceroll = defender.ThrowForDefence ();
				this.console.ConsoleWrite (string.Format (
					"\t\"{0}\" attack rolls {1} at a distance of {2:##.#} with range {3}", 
					attacker.Name, attackroll, dist, range));
				this.console.ConsoleWrite (string.Format (
					" : \"{0}\" defence rolls {1}", defender.Name, defenceroll));
				if (attackroll >= defenceroll)
				{
					stats.TotalHits++;
					this.console.ConsoleWrite (" HIT");
					int power = attacker.RollDamage ();
					this.console.ConsoleWrite (string.Format (
                        " for {0} damage", power));
					int armor = defender.GetAttribute("Armor").Current;
                    this.damage_done = armor - power;
                    if (this.damage_done < 0)
					{
                        stats.TotalDamage += (-1 * this.damage_done);
                        defender.TakeDamage (this.damage_done);
						this.console.ConsoleWriteLine(string.Format(
							" \"{0}\" took {1} damage leaving {2} hp", 
                            defender.Name, this.damage_done, defender.HitPoints));
					}
					else
					{
						this.console.ConsoleWriteLine(string.Format(
							" \"{0}\" took no damage \"{1}\" could not penetrate armor of {2}", 
							defender.Name, attacker.Name, 
                            defender.GetAttribute ("Armor").Current));
					}					
				}
				else
				{
					stats.TotalMisses++;
					this.console.ConsoleWriteLine (" MISSED");
				}
			}
			else
			{
                // TODO: this should not be done here but in the determine range method
				this.console.ConsoleWriteLine (string.Format (
					"\tOUT OF RANGE: \"{0}\" is {1:0.0} meters away from \"{2}\"", 
					defender.Name, dist, attacker.Name));
				int move = attacker.GetAttribute ("Speed").Current;				
				this.console.ConsoleWriteLine (string.Format ("{0} closing {1} meters", 
					attacker.Name, move));
				this.gamemap.MovePlayerToTarget (attacker, move, defender);
			}
            return this.damage_done;
		}

        public Player Attacker { 
            get { 
                return this.attacker; 
            }
        }

        public Player Defender { 
            get { 
                return this.defender; 
            }
        }

        public int DamageDone 
        { 
            get { 
                return this.damage_done;
            }
        }

        public int TurnNumber
        {
            get {
                return this.turn;
            }
        }
	}
}

