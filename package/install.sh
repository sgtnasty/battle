#!/bin/bash

# the places we will be installing
BIN_PREFIX=/usr/local
BATTLE_HOME=$BIN_PREFIX/share/battle
BATTLE_BIN_HOME=$BIN_PREFIX/bin
BATTLE_SOURCES=../Battle/bin/Debug


# check if root
if [ $UID -ne 0 ]; then
    echo -e "\033[1;33m""This program must be run as root.""\033[0m"
    sleep 2
    exit 0
fi


echo -e "\033[1;33m""Creating battle directory in $BATTLE_HOME""\033[0m"
echo ""
install -v -d $BATTLE_HOME
install -v -d $BATTLE_HOME/Resources


echo -e "\033[1;33m""Installing battle files in $BATTLE_HOME""\033[0m"
echo ""
install -v $BATTLE_SOURCES/Battle.exe $BATTLE_HOME
install -v $BATTLE_SOURCES/Battle.exe.config $BATTLE_HOME
install -v $BATTLE_SOURCES/BattleEngine.dll $BATTLE_HOME
install -v $BATTLE_SOURCES/BattleGtk.dll $BATTLE_HOME
install -v $BATTLE_SOURCES/Resources/BattleGtk.ui $BATTLE_HOME/Resources
install -v $BATTLE_SOURCES/Resources/battleicon00.jpg $BATTLE_HOME/Resources
install -v $BATTLE_SOURCES/Resources/battleicon01.jpg $BATTLE_HOME/Resources


echo -e "\033[1;33m""Installing startup script in $BATTLE_BIN_HOME""\033[0m"
echo ""
install -v ./battle $BATTLE_BIN_HOME
cp ../Battle/app.desktop /usr/share/applications/battle.desktop


echo -e "\033[1;33m""Updating the desktop""\033[0m"
xdg-desktop-menu install --novendor /usr/share/applications/battle.desktop
