#!/bin/bash

# the places we will be installing
BIN_PREFIX=/usr/local
BATTLE_HOME=$BIN_PREFIX/share/battle
BATTLE_BIN_HOME=$BIN_PREFIX/bin
BATTLE_SOURCES=../Battle/bin/Debug


# check if root
if [ $UID -ne 0 ]; then
    echo -e "\033[1;33m""This program must be run as root.""\033[0m"
    sleep 2
    exit 0
fi


echo -e "\033[1;33m""Removing startup script in $BATTLE_BIN_HOME""\033[0m"
echo ""
rm "$BATTLE_BIN_HOME/battle"
rm /usr/share/applications/battle.desktop


echo -e "\033[1;33m""Removing battle files in $BATTLE_HOME""\033[0m"
echo ""
rm $BATTLE_HOME/Battle.exe
rm $BATTLE_HOME/Battle.exe.config
rm $BATTLE_HOME/BattleEngine.dll
rm $BATTLE_HOME/BattleGtk.dll
rm $BATTLE_HOME/Resources/BattleGtk.ui
rm $BATTLE_HOME/Resources/battleicon00.jpg
rm $BATTLE_HOME/Resources/battleicon01.jpg


echo -e "\033[1;33m""Removing battle directory in $BATTLE_HOME""\033[0m"
echo ""
rmdir $BATTLE_HOME/Resources
rmdir $BATTLE_HOME


echo -e "\033[1;33m""Updating the desktop""\033[0m"
xdg-desktop-menu uninstall /usr/share/applications/battle.desktop
